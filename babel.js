//全局环境下进行babel转码，这意味着，如果项目要运行，全局环境必须有 Babel，也就是说项目产生了对环境的依赖。另一方面，这样做也无法支持不同项目使用不同版本的 Babel。
//1、全局下载babel'
//npm install -global babel-cil
//3、下载
//# 最新转码规则
// npm install --save-dev babel-preset-latest

// react 转码规则（可不下载）
// npm install --save-dev babel-preset-react

// 不同阶段语法提案的转码规则（共有4个阶段），选装一个
// npm install --save-dev babel-preset-stage-0
// npm install --save-dev babel-preset-stage-1
// npm install --save-dev babel-preset-stage-2（选其中一个）
// npm install --save-dev babel-preset-stage-3
//2、配置.babelrc文件（注意，以下所有 Babel工具和模块的使用，都必须先写好.babelrc。）
//{
//	'presets:["latest","stage-2"]',//预先配置
//	'plugins:[]'//插件
//}
//输出
//# 转码结果输出到标准输出(直接在小黑板中输出)
//$ babel example.js
//
//# 转码结果写入一个文件
// # --out-file 或 -o 参数指定输出文件
// $ babel example.js --out-file compiled.js
// # 或者
// $ babel example.js -o compiled.js
// 
// # 整个目录转码
// # --out-dir 或 -d 参数指定输出目录
// $ babel src(文件夹的名字) --out-dir lib
// # 或者
// $ babel src -d lib
// 
// # -s 参数生成source map文件
//$ babel src -d lib -s
//
//一个解决（环境依赖）办法是将babel-cli安装在项目之中。
//# 安装$ npm install --save-dev babel-cli
// 然后，改写package.json。
// {
//   // ...
//   "devDependencies": {
//     "babel-cli": "^6.0.0"
//   },
//   "scripts": {
//     "build": "babel src -d lib"
//   },
// }
// 转码的时候，就执行下面的命令。
// $ npm run build

 
// babel-cli工具自带一个babel-node命令，提供一个支持ES6的REPL环境。它支持Node的REPL环境的所有功能，而且可以直接运行ES6代码。



// babel-register
// babel-register模块改写require命令，为它加上一个钩子。此后，每当使用require加载.js、.jsx、.es和.es6后缀名的文件，就会先用Babel进行转码。

// $ npm install --save-dev babel-register
// 使用时，必须首先加载babel-register。

// require("babel-register");
// require("./index.js");
// 然后，就不需要手动对index.js转码了。

// 需要注意的是，babel-register只会对require命令加载的文件转码，而不会对当前文件转码。另外，由于它是实时转码，所以只适合在开发环境使用。
// 
