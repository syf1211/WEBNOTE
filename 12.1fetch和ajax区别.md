# Ajax
  Ajax本质是使用XMLHttpRequest对象来请求数据
  ``js
  function ajax(url, fnSucc, fnFaild){
    //1.创建Ajax对象
    if(window.XMLHttpRequest){
      var oAjax=new XMLHttpRequest();
    }else{
      var oAjax=new ActiveXObject("Microsoft.XMLHTTP");
    }

    //2.连接服务器（打开和服务器的连接）
    oAjax.open('GET', url, true);

    //3.发送
    oAjax.send();

    //4.接收
    oAjax.onreadystatechange=function (){
      if(oAjax.readyState==4){
          if(oAjax.status==200){
              //alert('成功了：'+oAjax.responseText);
              fnSucc(oAjax.responseText);
          }else{
              //alert('失败了');
              if(fnFaild){
                  fnFaild();
              }
          }
        }
    };
  }
  ``js

# fetch
  fetch是全局变量window的一个方法
  主要的特点：
  1、第一个参数是URL;
  2、第二个可选参数，可以控制不同的配置的init对象
  3、使用了Javascript Promise来处理结果/回调
  
  ****注意：
    fetch 的第二参数中
    1、默认的请求为get请求 可以使用method:post 来进行配置 
    2、第一步中的 response有许多方法 json() text() formData()
    3、Fetch跨域的时候默认不会带cookie 需要手动的指定 credentials:'include'
  ``js
    // 链式处理,将异步变为类似单线程的写法: 高级用法.
    fetch('/some/url').then(function(response) {
        response.json() . //... 执行成功, 第1步...
    }).then(function(returnedValue) {
        // ... 执行成功, 第2步...
    }).catch(function(err) {
        网络故障时或任何阻止请求完成时
        // 中途任何地方出错...在此处理 :( 
    });
  ``js
  除了传给 fetch() 一个资源的地址，你还可以通过使用 Request() 构造函数来创建一个 request 对象，然后再作为参数传给 fetch()：
    var myHeaders = new Headers();
    var myInit = { method: 'GET',
                  headers: myHeaders,
                  mode: 'cors',
                  cache: 'default' };
    var myRequest = new Request('flowers.jpg', myInit);
    fetch(myRequest).then(function(response) {
      return response.blob();
    }).then(function(myBlob) {
      var objectURL = URL.createObjectURL(myBlob);
      myImage.src = objectURL;
    });

##  Headers

  1、使用 Headers 的接口，你可以通过 Headers() 构造函数来创建一个你自己的 headers 对象。一个 headers 对象是一个简单的多名值对：
  例如：
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "text/plain");
  2、也可以传一个多维数组或者对象字面量：
    myHeaders = new Headers({
      "Content-Type": "text/plain",
      "Content-Length": content.length.toString(),
      "X-Custom-Header": "ProcessThisImmediately",
    });

##  Guard
    Headers 可以在 request 请求中被发送或者在 response 请求中被接收，并且规定了哪些参数是可写的，Headers 对象有一个特殊的 guard 属性。这个属性没有暴露给 Web，但是它影响到哪些内容可以在 Headers 对象中被操作。

##  Body
      1、不管是请求还是响应都能够包含body对象
      2、body也可以是以下任意类型的实例.
          ArrayBuffer
          ArrayBufferView (Uint8Array and friends)
          Blob/File
          string
          URLSearchParams
          FormData
    Body 类定义了以下方法 (这些方法都被 Request 和Response所实现)以获取body内容. 这些方法都会返回一个被解析后的promise对象和数据.
        arrayBuffer()
        blob()
        json()
        text()
        formData()

        ***比起XHR来，这些方法让非文本化的数据使用起来更加简单。

# ****1、fetch 规范与 jquery.ajax()主要有两种方式不同
    从 fetch()返回的 Promise 将不会拒绝HTTP错误状态, 即使响应是一个 HTTP 404 或 500。相反，它会正常解决 (其中ok状态设置为false), 并且仅在网络故障时或任何阻止请求完成时，它才会拒绝。
    2、默认 情况下，fetch在服务端不会发送或接受任何cookies，如果站点依赖于维护一个用户会话，则导致未经认证的请求（要发送cookie，必须发送凭据头）
    ****
    如果想要在同域中自动发送cookie,加上 credentials 的 same-origin 选项
    same-origin值使得fetch处理Cookie与XMLHttpRequest类似
    对于CORS请求，使用include值允许将凭据发送到其他域（
      fetch(url, {
        credentials: 'include'
      })
    ）