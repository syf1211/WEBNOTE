# 深入JSX
  本质上来讲，JSX 只是为 React.createElement(component, props, ...children)  方法提供的语法糖

  编译前
  <MyButton color="blue" shadowSize={2}>
    Click Me
  </MyButton>
  编译为：
  React.createElement(
    MyButton,
    {color: 'blue', shadowSize: 2},
    'Click Me'
  )

#指定 React 元素类型
  JSX 的标签名决定了 React 元素的类型。
  大写开头的 JSX 标签表示一个 React 组件。这些标签将会被编译为同名变量并被引用，所以如果你使用了 <Foo /> 表达式，则必须在作用域中先声明 Foo 变量。

#React 必须声明
  由于 JSX 编译后会调用 React.createElement 方法，所以在你的 JSX 代码中必须首先声明 React 变量。

  **如果你使用<script>加载React,它将作用于全局

#点表示法
  你还可以使用 JSX 中的点表示法来引用 React 组件。你可以方便地从一个模块中导出许多 React 组件。
  例如，有一个名为 MyComponents.DataPicker 的组件，你可以直接在 JSX 中使用它：
  import React from 'react';
  const MyComponents = {
    DatePicker: function DatePicker(props) {
      return <div>Imagine a {props.color} datepicker here.</div>;
    }
  }

  function BlueDatePicker() {
    return <MyComponents.DatePicker color="blue" />;
  }

  #首字母大写
    当元素类型以小写字母开头时，它表示一个内置组件
    以大写字母开头，它表示自定义组件