//1、简单介绍
//promise是异步编程的一种解决方案，比普通的回调函数和事件更强大
//所谓Promise，简单说就是一个容器，里面保存着某个未来才会结束的事件（通常是一个异步操作）的结果。从语法上说，Promise 是一个对象，从它可以获取异步操作的消息。
//

//2、promise的特点
//	（1）、对象的状态不会受外界影响==》//promise对象带便一个异步操作，有三种状态：pending(进行中)、fulfilled(已成功)、reject(已失败)===》也就是只有异步操作的结果可以决定当前是哪一种状态，任何其他操作都无法改变这个状态
//	（2）、一旦状态改变就不会再变，任何时候都可以得到这个结果==》对象改变只有两种可能，从pending变为fulfilled和从pending变为rejected。==》只要这两种情况发生，状态就凝固了，不会再变了，会一直保持这个结果，这时就称为 resolved（已定型）。如果改变已经发生了，你再对Promise对象添加回调函数，也会立即得到这个结果。
//	
//	3、romise的缺点
//	Promise也有一些缺点。首先，无法取消Promise，一旦新建它就会立即执行，无法中途取消。其次，如果不设置回调函数，Promise内部抛出的错误，不会反应到外部。第三，当处于pending状态时，无法得知目前进展到哪一个阶段（刚刚开始还是即将完成）。

// 4、基本用法
// es6规定，Promise对象是一个构造函数，用来生成Promise实例。构造函数接受两个参数，分别为resolve和reject.它们是两个函数，不用自己部署，有javascript引擎提供。
// resolve（异步操作成功时调用，并将结果作为参数传递过去）
// // reject（异步操作失败时调用，并将结果作为参数传递错误过去）
// Promise实例生成以后，可以用then方法分别指定resolved状态和rejected状态的回调函数。
// 例子：
// var promise = new Promise(function(resolve, reject) {
  // ... some code
//   if (/* 异步操作成功 */){
//     resolve(value);
//   } else {
//     reject(error);
//   }
// });
// (1)
// promise.then(function(value) {
//   // success
// }, function(error) {
//   // failure
// });
// (2)
// promise.then(result => console.log(result))
 // .catch(error => console.log(error))
// 
// promise实现Ajax操作的例子
// var getJSON = function(url) {
//   var promise = new Promise(function(resolve, reject){
//     var client = new XMLHttpRequest();
//     client.open("GET", url);
//     client.onreadystatechange = handler;
//     client.responseType = "json";
//     client.setRequestHeader("Accept", "application/json");
//     client.send();

//     function handler() {
//       if (this.readyState !== 4) {
//         return;
//       }
//       if (this.status === 200) {
//         resolve(this.response);
//       } else {
//         reject(new Error(this.statusText));
//       }
//     };
//   });

//   return promise;
// };

// getJSON("/posts.json").then(function(json) {
//   console.log('Contents: ' + json);
// }, function(error) {
//   console.error('出错了', error);
// });