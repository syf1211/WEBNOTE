//Vue（类似view）是一套构建用户界面的渐进式框架
				//它与别的框架不同，它是 采用自底向上增量开发的设计。
				//
//一个 Vue 应用由一个通过 new Vue 创建的根 Vue 实例，以及可选的嵌套的、可复用的组件树组成。

//核心:只关注视图层、采用简洁的模板语法来声明式的将数据渲染进DOM的系统
//ng-bind
//为什么是响应式:时时更新数据，所以是响应式（get/set方法进行数据双向绑定，这边改变另一边就会跟着改变）
///
//指令带有前缀 v-，以表示它们是 Vue 提供的特殊属性。
//不仅可以绑定 DOM 文本到数据，也可以绑定 DOM 结构到数据
//每个 Vue 应用都是通过 Vue 函数创建一个新的 Vue 实例开始的：
//数据绑定最常见的形式就是使用“Mustache”语法 (双大括号) 的文本插值
//
//Vue.js 都提供了完全的 JavaScript 表达式支持（有个限制就是，每个绑定都只能包含单个表达式）
//slot插槽
1、v-bind:  动态绑定dom属性值(你在控制台或者别的地方改变这个值它也会随着改变)
	放在组件上（和组件component结合用）是传参的意思（在组件里用props接受参数）
缩写 :
2、v-on:  绑定事件处理函数
缩写 @
3、v-html='' //输出html结构
4、v-if='变量'	//条件语句
5、v-once	//执行一次性地插值，当数据改变时，插值处的内容不会更新
6、v-model=''	//实现表单输入和应用状态之间的双向绑定
7、v-for='(item,index) 调用方法in 要循环的数组'	//循环
8、v-on:click='' //监听器用来(为了让用户和你的应用进行互动)
9、组件
	// 定义名为 todo-item 的新组件
	Vue.component('todo-item', {
	// "prop"，类似于一个自定义属性
  	// 这个属性名为 todo。
  	  props: ['todo'],
	  template: '<li>这是个待办项</li>'
	})
	现在你可以用它构建另一个组件模板：
	<ol>
	  <!-- 创建一个 todo-item 组件的实例 -->
	  <todo-item></todo-item>
	</ol>

10、计算属性
	当执行的数据一样时，它只会执行一次，结果是执行之前缓冲的值
	这就是对于任何复杂逻辑，你都应当使用计算属性的原因。
	computer:{写方法（你要实实现的效果（属性名字可以随意写））}//计算属性（只执行一次，返回上一次缓冲执行的值，依赖的值发生变化，它就随着变化，）
	计算属性是基于
	实现原理；利用了闭包。
	computed监听的是函数内部依赖的数据
11、修饰符

	修饰符 (Modifiers) 是以半角句号 . 指明的特殊后缀，用于指出一个
	.stop//默认冒泡
	.prevent默认行为
	.self
	.once
	.capture
	指令应该以特殊方式绑定。例如，.prevent 修饰符告诉 v-on 指令对于触发的事件调用 event.preventDefault()：
	<form v-on:submit.prevent="onSubmit"></form>
12、watch: {//方法的形式（检测变化）
		msg: function(newVal, oldVal){//接受两个参数
		//监听谁，方法名写谁
	}
	//针对某一个属性进行监听
}

<template v-if='true'></template>//不会被渲染出来
key 重复元素不复用

v-show=''||v-hide=''//显示隐藏(显示隐藏)
v-if='';//惰性的，为假的时候什么都不做
v-for 和v-if一起用的v-for优先级比较高



html

<div id="app" v-bind:title="msg">
	<p v-if="1+2 != 3">傻子也知道</p>
	<p v-else-if="1+2 == 3">我天这你都会</p>
	<p v-else>哎</p>
	<p v-once>{{msg}}</p>
	<span v-html="msgHtml" ></span>
	{{msg}} <!--声明式的语法渲染数据-->
	<input v-model="msg">
	<ul>
		<li v-for="item in arr">{{item}}</li>
	</ul>
	<ul>
		<li v-for="(item,index) in arr2">{{index}}-{{item.name}}</li>
	</ul>
	<a href="#" v-bind:title="flg">asadsdasd</a>
	<button v-on:click="addS($event)">添加个S</button>
	<todo-item v-bind:sss="flg"  v-bind:ssss="flg"></todo-item>
</div>
html

// 定义名为 todo-item 的新组件
	Vue.component('todo-item', {
	  props: ['sss'], //props里面的数据是接收的参数
	  template: '<li>这是个待办项</li>'
	})
	//实例化Vue函数
	var myApp = new Vue({//这里面所有的this都代表myApp
		el: "#app", //将vue实例挂载到dom上去
		//data在vue中指的是当前组件的数据（专门放数据的）
		data: {
			msg: '我哦是诗句',
			flg: true,
			aaa: 100,
			msgHtml:"<div>asdasdasd</div>",
			arr: [
				's1','s12','s13','s14','s15','s16','s17','s18'
			],
			arr2: [
				{name:'xxxx1'},
				{name:'xxxx2'},
				{name:'xxxx3'},
				{name:'xxxx4'}
			]
		},

		//所有的方法函数都放到methods里面
		methods: {
			addS: function(e){
				console.log(e)
				this.arr.push( 's' )
			}
		},
		computer:{//计算属性
			
		},
		watch: {//方法的形式（检测变化）
			msg: function(newVal, oldVal){//接受两个参数
			//监听谁，方法名写谁
		}
		created : function(){  //实例被创建完，不能操作dom，只能操作数据,这是在渲染前最后一次修改数据的机会
			console.log(  this.$el )
		},

		mounted: function(){  //dom挂载完成后，可以在这里对dom进行操作（el 被新创建的 vm.$el 替换，并挂载到实例上去之后调用该钩子。）
			console.log( this.$el )
		},

		updated: function(){  //当数据的改变引起了dom的变化才会执行这个钩子
			console.log( '更新的时候i' )
		},



	})
	// setTimeout(function(){
	// 	myApp.$mount('#app')  //el的替代方案
	// },2000)
	
	console.log( myApp.$data )



闭包：
	闭包就是函数套函数，内部函数可以访问外部的函数的变量，不污染全局，所以需要把这个计算过程封装在闭包中。
	再比如，定义了一个插件或者框架，都需要用到闭包。让