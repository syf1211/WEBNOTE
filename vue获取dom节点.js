Vue获取DOM元素样式 && 样式更改
在 vue 中用 document 获取 dom 节点进行节点样式更改的时候有可能会出现 'style' is not definde的错误，

这时候可以在 mounted 里用 $refs 来获取样式，并进行更改：

<template>
  <div style="display: block;" ref="abc">
    <!-- ... -->
  </div>
</template>
<script>
　　export default {
  　　mounted () {
   　　 console.log(this.$refs.abc.style.cssText)
  　　}
　　}
</script>