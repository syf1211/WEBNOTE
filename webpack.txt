webpack 模块打包器
入口文件webpack.config.js
导出
默认
export default 'a.js'
导入
import a from '路径’
具名导出
exprot const obj = {
	a: 'a.js'
}
import {obj} from '路径'
import a， {obj} from '路径’
import {obj as params} from '路径' 修改具名

export {
	obj as params
}
import {params} from ''
将所有导出内容转为对象 import * as obj from ''
模块
js模块
css模块
html模块

js和json默认

html-webpack-plugin 插件
module.exports = {
	entry: '入口文件',
	output: {
		path: '路径',
		filename: '名称',
		// publicPath: path.join(__dirname, 'assest/') //指定静态资源的位置
	},
	module: { // 在配置文件里面添加loader
		rules: [
			{
				test: /\.js$/,
				use: ['babel-loader'],
				include: [path.join(__dirname, 'app')],
				exclude: /node_modules/
			},
			{
				test: /\.vue$/,
				use: ['vue-loader']
			},
			{
				test: /\.css$/,
				use: ['style-loader','css-loader']
			},
			{
				test: /\.less$/,
				use: ['style-loader','css-loader','less-loader']
			},
			{
				test: /\.scss$/,
				use: ['style-loader','css-loader','sass-loader']
			},
			{
				test: /\.(jpg|png|gif)$/,
				use: {
					loader: 'url-loader',
					options: [
						limit: 1000
						methods: true // 转成对象 选择器可以使用 标签不可以
					]
				}
			},
			{
				test: /\.woff$/,
				use: {
					loader: 'url-loader',
					options: {
						limit: 1000
					}
				}
			}
		]
	},
	resolveLoader: {// 可以省略 -loader不写
		moduleExtensions: ['-loader']
	},
	resolve:{
		alias: {
			'vue$':'vue/dist/vue.common.js'
		}
	},
	plugins: [ // 自动刷新
		new HtmlWebpackPluginz({
			filename: 'index.html',
			template: path.join(__dirname, 'index.html'),
			inject: 'body' 或 true 
		}),
		new webpack.BannerPlugin('1507B'), //给打包的文佳加一条注释
		new webpack.DefinePlugin({
			'process.env': {
				NOOD_ENV: '"dev"'  //env 环境变量
			} //dev 测试（开发）环境
		})
	],
	devServer: {   //webpack-dev-server 静态资源
		hot: true,  --hot 热更新  局部刷新 不刷新浏览器
		inline: true, --inline 加了个iframe
		port: 3000  刷新浏览器
	}
}
--open 自动打开页面
node版本8.6支持import

npm install webpack -g
新建你的项目文件夹。  有packagejson    再下载webpack  npm install webpack -D

书写  webpack.config.js     entry  output