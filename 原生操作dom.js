<span style="font-size:18px;">/** 
 * Created by hua on 2014/10/21. 
 */  
//查找节点  
document.getElementById("id");//通过id查找，返回唯一的节点，如果有多个将会返回第一个，在IE6、7中有个bug，会返回name值相同的元素，所有要做一个兼容  
document.getElementsByClassName("class");//通过class查找，返回节点数组  
document.getElementsByTagName("div");  
  
//创建节点  
document.createDocumentFragment();//创建内存文档碎片  
document.createElement();//创建元素  
document.createTextNode();//创建文本节点  
  
//添加节  
var ele = document.getElementById("my_div");  
var oldEle = document.createElement("p");  
var newEle=document.createElement("div");  
ele.appendChild(oldEle);  
//移除  
ele.removeChild(oldEle);  
//替换  
ele.replaceChild(newEle,oldEle);  
;插入  
ele.insertBefore(oldEle,newEle);//在newEle之前插入 oldEle节点  
  
//复制节点  
var cEle = oldEle.cloneNode(true);//深度复制，复制节点下面所有的子节点  
cEle = oldEle.cloneNode(false);//只复制当前节点，不复制子节点  
  
//移动节点  
var cloneEle = oldEle.cloneNode(true);//被移动的节点  
document.removeChild(oldEle);//删除原节点  
document.insertBefore(cloneEle,newEle);//插入到目标节点之前  
 
</span>  

